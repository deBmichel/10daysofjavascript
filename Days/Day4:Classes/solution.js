'use strict';

class Polygon {
    constructor(lades) {
        this.lades = lades;
    }
    perimeter() {
    	let count = 0;
    	for (let lade in this.lades) {
    		count = count + this.lades[lade];
    	}
        return count;
    }
}

const rectangle = new Polygon([10, 20, 10, 20]);
const square = new Polygon([10, 10, 10, 10]);
const pentagon = new Polygon([10, 20, 30, 40, 43]);

console.log(rectangle.perimeter());
console.log(square.perimeter());
console.log(pentagon.perimeter());