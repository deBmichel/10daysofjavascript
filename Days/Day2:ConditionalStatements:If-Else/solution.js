'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

function getGrade(score) {
    // Write your code here
    let grade;
    let intScore = parseInt(score);
    //The switch in js needs to be of the same tipe to function
    switch ( true ) {
        case ((intScore > 25) && (intScore <= 30)):
            grade = "A";
            break;
        case ((intScore > 20) && (intScore <= 25)):
            grade = "B";
            break;
        case ((intScore > 15) && (intScore <= 20)):
            grade = "C";
            break;
        case ((intScore > 10) && (intScore <= 15)):
            grade = "D";
            break;
        case (( intScore > 5) && (intScore <= 10)):
            grade = "E";
            break;
        case (( intScore > 0) && (intScore <= 5)):
            grade = "F";
            break;
        default:
            grade = "NoGrade";
            console.log("No grade");
    }
    return grade;
}


function main() {
    const score = +(readLine());
    
    console.log(getGrade(score));
}