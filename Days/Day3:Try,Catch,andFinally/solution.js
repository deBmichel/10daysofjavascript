/*
	URLProblem : https://www.hackerrank.com/challenges/js10-try-catch-and-finally/problem
	Url Tutorial: 
*/
'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the reverseString function
 * Use console.log() to print to stdout.
 */
function reverseString(s) {
	try {
		var originarr = s.split("");
		var reversarr =originarr.reverse();
		var newstr = reversarr.join("");
		console.log(newstr);
	} catch (e) {
		console.log(e.message+"\n"+s);
	}   
}


function main() {
    const s = eval(readLine());
    
    reverseString(s);
}
