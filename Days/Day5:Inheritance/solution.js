'use strict';

class Rectangle {
    constructor(w, h) {
        this.w = w;
        this.h = h;
    }
}

/*
 *  Write code that adds an 'area' method to the Rectangle class' prototype
 */
let thearea = function() {
    if (this.h == undefined) {
        return this.w * this.w;
    } else {
        return this.w * this.h;
    }    
}
Rectangle.prototype.area = thearea;

/*
 * Create a Square class that inherits from Rectangle and implement its class constructor
 */
class Square extends Rectangle {
    //Lesson JS is dangerous for my mind , review the let thearea ??? JS 
    //Pass the context without questions implicit all ???
}

if (JSON.stringify(Object.getOwnPropertyNames(Square.prototype)) === JSON.stringify([ 'constructor' ])) {
    const rec = new Rectangle(3, 4);
    const sqr = new Square(3);
    
    console.log(rec.area());
    console.log(sqr.area());
} else {
    console.log(-1);
    console.log(-1);
}