'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

function getMaxLessThanK(n, k) {
    let limit = parseInt(n);
    let mayor = 0;

    let indxlimit = 1;
    while (indxlimit < limit) {

        let intrnlidx = indxlimit + 1;
        while (intrnlidx <= limit ) {
            let binaryOperation = indxlimit & intrnlidx;

            if (binaryOperation > mayor && binaryOperation < k){
                mayor = binaryOperation
            }
            
            intrnlidx = intrnlidx + 1;
        }

        indxlimit = indxlimit + 1;
    }

    return mayor;
}

function main() {
    const q = +(readLine());
    
    for (let i = 0; i < q; i++) {
        const [n, k] = readLine().split(' ').map(Number);
        
        console.log(getMaxLessThanK(n, k));
    }
}