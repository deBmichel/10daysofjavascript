/*
URLProblem: https://www.hackerrank.com/challenges/js10-data-types/problem


In tutorial https://www.hackerrank.com/challenges/js10-data-types/topics/javascript-data-types

You can find infomation about datatypes:
Number
String
Boolean
Symbol
Null
Undefined
The seventh data type is Object

You will learn than:
1 / 0 = Infinity
1 / -0 = -Infinity
MAX_VALUE: 1.7976931348623157e+308
MAX_VALUE + 1 = Infinity
MIN_VALUE = 5e-324
NaN
MAX_SAFE_INTEGER = 9007199254740991
MIN_SAFE_INTEGER = -9007199254740991
SquareRoot(-1) = NaN
isSafeInteger(100) = true

150000 is a number
Hello is a string
true is a boolean
Symbol(some Symbol variable) is a symbol
null is an object
undefined is an undefined
[object Object] is an object
NaN is a number

Coercion
In JavaScript, you can perform operations on values of different types without raising an exception. The JavaScript interpreter implicitly converts, or coerces, one of the data types to that of the other, then performs the operation. The rules for coercion of string, number, or boolean values are as follows:

If you add a number and a string, the number is coerced to a string.
If you add a boolean and a string, the boolean is coerced to a string.
If you add a number and a boolean, the boolean is coerced to a number.


For this exercice the tutorial doesnot speak about parseFunctions()
*/

'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

/**
*   The variables 'firstInteger', 'firstDecimal', and 'firstString' are declared for you -- do not modify them.
*   Print three lines:
*   1. The sum of 'firstInteger' and the Number representation of 'secondInteger'.
*   2. The sum of 'firstDecimal' and the Number representation of 'secondDecimal'.
*   3. The concatenation of 'firstString' and 'secondString' ('firstString' must be first).
*
*	Parameter(s):
*   secondInteger - The string representation of an integer.
*   secondDecimal - The string representation of a floating-point number.
*   secondString - A string consisting of one or more space-separated words.
**/
function performOperation(secondInteger, secondDecimal, secondString) {
    // Declare a variable named 'firstInteger' and initialize with integer value 4.
    const firstInteger = 4;
    
    // Declare a variable named 'firstDecimal' and initialize with floating-point value 4.0.
    const firstDecimal = 4.0;
    
    // Declare a variable named 'firstString' and initialize with the string "HackerRank".
    const firstString = 'HackerRank ';
    
    // Write code that uses console.log to print the sum of the 'firstInteger' and 'secondInteger' (converted to a Number        type) on a new line.
    var intsum = firstInteger + parseInt(secondInteger);
    console.log(intsum);
    // Write code that uses console.log to print the sum of 'firstDecimal' and 'secondDecimal' (converted to a Number            type) on a new line.
    var decsum = firstDecimal + parseFloat(secondDecimal);
    console.log(decsum);
    // Write code that uses console.log to print the concatenation of 'firstString' and 'secondString' on a new line. The        variable 'firstString' must be printed first.
    var strsum = firstString + secondString;
    console.log(strsum);
}


function main() {
    const secondInteger = readLine();
    const secondDecimal = readLine();
    const secondString = readLine();
    
    performOperation(secondInteger, secondDecimal, secondString);
}


