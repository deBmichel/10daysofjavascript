/*
ProblemUrl: https://www.hackerrank.com/challenges/js10-loops/problem?h_r=next-challenge&h_v=zen
Tutorial: https://www.hackerrank.com/challenges/js10-loops/topics/javascript-loops
JS loop types:
for
while
do-while
for-in
for-of

is very interesting the topic with (=) (==) and (===) to compare equality in js
The tuto does not speak about if else control structures but the exercise is interesting.
*/

'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });
    
    main();    
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the vowelsAndConsonants function.
 * Print your output using 'console.log()'.
 */
function vowelsAndConsonants(s) {
	var consonants = "";
	for (let char of s) {
		if (char == "a" || char == "e" || char == "i" || char == "o" || char == "u") {
			console.log(char);
		} else {
			if (char == "A" || char == "E" || char == "I" || char == "O" || char == "U") {
				console.log(char);
			} else {
				consonants = consonants + char;
			}
		}
	}

	for (let char of consonants) {
		console.log(char);
	}
}

function main() {
    const s = readLine();
    
    vowelsAndConsonants(s);
}


