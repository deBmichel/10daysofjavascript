document.body.innerHTML = ` 
	<div id="res"></div>
	<div id="btns">
		<button id="btn0" class="btn" onclick="addNum('0')">0</button>
		<button id="btn1" class="btn" onclick="addNum('1')">1</button>
		<button id="btnClr" class="btn" onclick="clearRESAREA()">C</button>
		<button id="btnEql" class="btn" onclick="showResCalc()">=</button>
		<button id="btnSum" class="btn" onclick="addSignOp('+')">+</button>
		<button id="btnSub" class="btn" onclick="addSignOp('-')">-</button>
		<button id="btnMul" class="btn" onclick="addSignOp('*')">*</button>
		<button id="btnDiv" class="btn" onclick="addSignOp('/')">/</button>
	</div>
`;

const RESAREA = document.getElementById("res");
const TYPE_SIGN = "TYPE_SIGN";
const TYPE_NUMB = "TYPE_NUMB";

var calExprss = "";
var flagCalExprssNum = false;
var flagCalExprssOpn = false;

let moveCalExpres = (type, sign) =>{
	if (type == TYPE_SIGN){
		if (sign !== "=") {			
			if (flagCalExprssNum) {
				calExprss = calExprss + "', 2) " + sign + " ";
				flagCalExprssNum = false;
			} else {
				calExprss = calExprss + sign;
			}
		} else {
			if (flagCalExprssNum) {
				calExprss = calExprss + "', 2) "
			}
		}
	}

	if (type == TYPE_NUMB){
		if (flagCalExprssNum) {
			calExprss = calExprss + sign;
		} else {
			calExprss = calExprss + "parseInt('" + sign;
			flagCalExprssNum = true;
		}
	}
}

let execCalc = () => {
	moveCalExpres(TYPE_SIGN, "=");
	let res = eval(calExprss).toString(2);
	clearRESAREA();
	calExprss = "parseInt('"+res+"', 2) "
	return res;
}

let addSignOp = (newSign) => {
	moveCalExpres(TYPE_SIGN, newSign);
	addSignInRes(newSign);
};

let addNum = (newNum) => {
	moveCalExpres(TYPE_NUMB, newNum);
	addSignInRes(newNum);
}

let addSignInRes = (newSign) => {RESAREA.innerHTML = (RESAREA.innerHTML + newSign);}

let clearRESAREA = () => {RESAREA.innerHTML=""; calExprss = ""; flagCalExprssNum = false;};

let showResCalc = () => RESAREA.innerHTML=execCalc();
